
import 'promise-addition';
import 'whatwg-fetch';

export default async function ajax({ url, body, ...options }) {
  if (body) {
    // 浅复制一次 body（不用深度复制）。对于 Date 类型，转为 Unix 时间（timestamp）
    body = Object.keys(body).reduce((result, key) => {
      let val = body[key];
      if (val instanceof Date) val = val.getTime();
      result[key] = val;
      return result;
    }, {});
    body = JSON.stringify(body);
  } else body = undefined;

  options = { method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
    credentials: 'include', // 'same-origin', see https://github.com/github/fetch#sending-cookies
    redirect: 'manual',
    ...options,
  };

  let result;
  try {
    const resp = await fetch(url, options).timeout(60000, '请求超时');

    if (resp.status < 200 || resp.status >= 300) {
      const text = await resp.text();
      result = { errno: resp.status, errmsg: text || resp.statusText };
    } else {
      result = await resp.json();
    }
  } catch (e) {
    let msg = e.message.toLowerCase();
    if (msg.indexOf('network request failed') > -1 || msg.indexOf('failed to fetch') > -1) {
      msg = '网络异常，请稍后再试。';
    }
    result = { errno: 500, errmsg: msg };
  }

  return result;
}
